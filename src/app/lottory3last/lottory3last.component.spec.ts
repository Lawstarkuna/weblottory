import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lottory3lastComponent } from './lottory3last.component';

describe('Lottory3lastComponent', () => {
  let component: Lottory3lastComponent;
  let fixture: ComponentFixture<Lottory3lastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lottory3lastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lottory3lastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
