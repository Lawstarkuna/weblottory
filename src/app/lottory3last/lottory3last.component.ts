import { Component, OnInit,ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  number: string;
  total: number;
}

@Component({
  selector: 'app-lottory3last',
  templateUrl: './lottory3last.component.html',
  styleUrls: ['./lottory3last.component.css']
})
export class Lottory3lastComponent implements OnInit {

  lottory_3last_number
  lottory_3last_count

  displayedColumns: string[] = ['number', 'total'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  listDatalottory: MatTableDataSource<any>;
  constructor(public http:HttpClient) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() {
    this.getlottory3last();
    this.dataSource.paginator = this.paginator;
  }
  getlottory3last(){
    this.http.get<any>('http://178.128.111.212:777/lotterydata3lastcount').subscribe(data=>{
      // alert(JSON.stringify(data))
      this.lottory_3last_number = JSON.parse(data.lotterydata3lastcount1)
      this.lottory_3last_count = JSON.parse(data.lotterydata3lastcount2)
      // console.log(this.lottory_3last_number)
      // console.log(this.lottory_3last_count)

    })
  }

}
const ELEMENT_DATA: PeriodicElement[] = [
  {number: '375', total: 4},
  {number: '008', total: 4},
  {number: '477', total: 3},
  {number: '876', total: 3},
  {number: '374', total: 3},
  {number: '775', total: 3},
  {number: '639',  total: 3},
  {number: '830', total: 3},
  {number: '074', total: 3},
  {number: '285', total: 3},
  {number: '362', total: 2},
  {number: '650', total: 2},
  {number: '648', total: 2},
  {number: '148', total: 2},
  {number: '132', total: 2},
  {number: '329', total: 2},
  {number: '127', total: 2},
  {number: '638', total: 2},
  {number: '159',  total: 2},
  {number: '628', total: 2}
];



