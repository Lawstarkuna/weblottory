import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lottory2lastComponent } from './lottory2last.component';

describe('Lottory2lastComponent', () => {
  let component: Lottory2lastComponent;
  let fixture: ComponentFixture<Lottory2lastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lottory2lastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lottory2lastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
