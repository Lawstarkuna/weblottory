import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  number: number;
  total: number;

}

@Component({
  selector: 'app-lottory2last',
  templateUrl: './lottory2last.component.html',
  styleUrls: ['./lottory2last.component.css']
})
export class Lottory2lastComponent implements OnInit {

  lottory_2last_number
  lottory_2last_count
  constructor(public http: HttpClient) { }
  

  displayedColumns: string[] = ['number', 'total'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  listDatalottory: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;



  ngOnInit() {
    this.getlottory2last();
    this.dataSource.paginator = this.paginator;

  }

  getlottory2last() {
    this.http.get<any>('http://178.128.111.212:777/lotterydata2lastcount').subscribe(data => {
      // alert(JSON.stringify(data))
      this.lottory_2last_number = JSON.parse(data.lotterydata2lastcount1)
      this.lottory_2last_count = JSON.parse(data.lotterydata2lastcount2)
      // console.log(this.lottory_2last_number)
    })
  }
  
}
const ELEMENT_DATA: PeriodicElement[] = [
  {number: 79, total: 8},
  {number: 85, total: 6},
  {number: 86, total: 5},
  {number: 65, total: 5},
  {number: 62, total: 5},
  {number: 98, total: 5},
  {number: 2,  total: 5},
  {number: 52, total: 5},
  {number: 26, total: 4},
  {number: 20, total: 4},
  {number: 28, total: 4},
  {number: 46, total: 4},
  {number: 53, total: 4},
  {number: 14, total: 4},
  {number: 95, total: 4},
  {number: 44, total: 4},
  {number: 35, total: 4},
  {number: 11, total: 3},
  {number: 9,  total: 3},
  {number: 51, total: 3}
];
