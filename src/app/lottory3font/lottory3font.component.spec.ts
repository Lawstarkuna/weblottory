import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lottory3fontComponent } from './lottory3font.component';

describe('Lottory3fontComponent', () => {
  let component: Lottory3fontComponent;
  let fixture: ComponentFixture<Lottory3fontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lottory3fontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lottory3fontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
