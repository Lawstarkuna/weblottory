import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  number: number;
  total: number;
}

@Component({
  selector: 'app-lottory3font',
  templateUrl: './lottory3font.component.html',
  styleUrls: ['./lottory3font.component.css']
  
})
export class Lottory3fontComponent implements OnInit {

  lottory_3font_number
  lottory_3font_count

  displayedColumns: string[] = ['number', 'total'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  listDatalottory: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public http:HttpClient) { }

  ngOnInit() {
    this.getlottory3font();
    this.dataSource.paginator = this.paginator;
  }

  getlottory3font(){
    this.http.get<any>('http://178.128.111.212:777/lotterydata3fontcount').subscribe(data=>{
      // alert(JSON.stringify(data))
      this.lottory_3font_number = JSON.parse(data.lotterydata3fontcount1)
      this.lottory_3font_count = JSON.parse(data.lotterydata3fontcount2)
      // console.log(this.lottory_3font_number)
      // console.log(this.lottory_3font_count)

    })
  }

}
const ELEMENT_DATA: PeriodicElement[] = [
  {number: 576, total: 5},
  {number: 589, total: 3},
  {number: 864, total: 3},
  {number: 912, total: 3},
  {number: 238, total: 5},
  {number: 945, total: 5},
  {number: 625,  total: 5},
  {number: 324, total: 5},
  {number: 227, total: 3},
  {number: 390, total: 3},
  {number: 784, total: 3},
  {number: 403, total: 3},
  {number: 530, total: 3},
  {number: 380, total: 3},
  {number: 779, total: 3},
  {number: 228, total: 3},
  {number: 235, total: 2},
  {number: 239, total: 2},
  {number: 242,  total: 2},
  {number: 250, total: 2}
];

