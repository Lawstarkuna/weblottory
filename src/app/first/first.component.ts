import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  lottoryno1:any;
  lottory2last:any;
  lottory3font1:any;
  lottory3font2:any;
  lottory3last1:any;
  lottory3last2:any;
  
  constructor(public http:HttpClient) { }

   ngOnInit() {

    this.getlottoryno1();
    this.getlottory2last();
    this.getlottory3font();
    this.getlottory3last();


  }
  getlottoryno1(){
    this.http.get<any>('http://178.128.111.212:777/lotteryno1').subscribe(data=>{
      // alert(JSON.stringify(data))
      let lottoryno_1 = JSON.parse(data.lotteryNo1)
      this.lottoryno1 = lottoryno_1[0];
    })
  }
  getlottory2last(){
    this.http.get<any>('http://178.128.111.212:777/lottery2last').subscribe(data=>{
      // alert(JSON.stringify(data))
      let lottory_2last = JSON.parse(data.lottery2last)
      this.lottory2last = lottory_2last[0];
    })
  }
  getlottory3font(){
    this.http.get<any>('http://178.128.111.212:777/lottery3font').subscribe(data=>{
      // alert(JSON.stringify(data))
      let lottory_3font = JSON.parse(data.lottery3font)
      this.lottory3font1 = lottory_3font[0];
      this.lottory3font2 = lottory_3font[1];
    })
  }
  getlottory3last(){
    this.http.get<any>('http://178.128.111.212:777/lottery3last').subscribe(data=>{
      // alert(JSON.stringify(data))
      let lottory_3last = JSON.parse(data.lottery3last)
      this.lottory3last1 = lottory_3last[0];
      this.lottory3last2 = lottory_3last[1];
    })
  }
}
